package pl.sda.tableperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "office_emp_tableperclass")
public class OfficeEmployee extends Employee {

    private String skills;

    public OfficeEmployee() {

    }

    public OfficeEmployee(String firstName, String lastName, String skills) {
        super(firstName, lastName);
        this.skills = skills;
    }
}
