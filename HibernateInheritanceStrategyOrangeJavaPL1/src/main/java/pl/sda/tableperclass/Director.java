package pl.sda.tableperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;


@Setter
@Getter
@Entity
@Table(name = "director_tableperclass")
public class Director extends Employee {

    private String department;

    public Director() {

    }

    public Director(String firstName, String lastName, String department) {
        super(firstName, lastName);
        this.department = department;
    }
}
