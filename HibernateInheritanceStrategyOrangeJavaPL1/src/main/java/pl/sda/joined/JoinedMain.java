package pl.sda.joined;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class JoinedMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV3 officeEmployee = new OfficeEmployeeV3("Jan", "Kowalski", "Excel");

        EmployeeV3 director = new DirectorV3("Michał", "Nowak", "IT");

        session.save(officeEmployee);
        session.save(director);

        OfficeEmployeeV3 officeEmployeeFromDB = session.get(OfficeEmployeeV3.class, 1);
        System.out.println(officeEmployeeFromDB.getSkills());


        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
