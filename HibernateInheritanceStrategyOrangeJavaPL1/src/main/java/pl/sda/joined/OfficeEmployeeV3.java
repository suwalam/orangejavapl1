package pl.sda.joined;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "office_emp_joined")
@PrimaryKeyJoinColumn(name = "id")
public class OfficeEmployeeV3 extends EmployeeV3 {

    private String skills;

    public OfficeEmployeeV3() {

    }

    public OfficeEmployeeV3(String firstName, String lastName, String skills) {
        super(firstName, lastName);
        this.skills = skills;
    }
}
