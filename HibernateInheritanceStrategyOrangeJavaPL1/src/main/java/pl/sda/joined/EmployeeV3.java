package pl.sda.joined;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "employee_joined")
public abstract class EmployeeV3 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    protected String firstName;

    protected String lastName;

    public EmployeeV3() {

    }

    public EmployeeV3(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
