package pl.sda.joined;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "director_joined")
@PrimaryKeyJoinColumn(name = "id")
public class DirectorV3 extends EmployeeV3 {

    private String department;

    public DirectorV3() {

    }

    public DirectorV3(String firstName, String lastName, String department) {
        super(firstName, lastName);
        this.department = department;
    }

}
