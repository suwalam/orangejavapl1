package pl.sda.mappedsuperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class Beverage extends Product {

    private Double capacity;

    public Beverage() {

    }

    public Beverage(String name, BigDecimal price, Double capacity) {
        super(name, price);
        this.capacity = capacity;
    }
}
