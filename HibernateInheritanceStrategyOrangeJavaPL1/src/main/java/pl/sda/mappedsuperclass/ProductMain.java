package pl.sda.mappedsuperclass;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.math.BigDecimal;
import java.util.List;

public class ProductMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Product cola = new Beverage("cola", BigDecimal.valueOf(8.49), 2.5);
        Product cola2 = new Beverage("cola", BigDecimal.valueOf(3.15), 0.5);
        Product apple = new Beverage("apple", BigDecimal.valueOf(4.67), 1.5);
        Product water = new Beverage("water", BigDecimal.valueOf(2.39), 1.75);


        Product copyCola = new Beverage(cola.getName(), cola.getPrice(), 2.5);
        session.save(cola);
        session.save(copyCola);
        session.save(cola2);
        session.save(apple);
        session.save(water);

        Beverage beverageFromDB = session.get(Beverage.class, 3);
        System.out.println(beverageFromDB.getName());

        transaction.commit();
        session.close();

        Session session2 = sessionFactory.openSession();
        Transaction transaction2 = session2.beginTransaction();

        session2.save(cola);

        String selectAllBeverage = "FROM Beverage";
        Query<Beverage> query = session2.createQuery(selectAllBeverage);
        List<Beverage> beverageList = query.getResultList();

        beverageList.stream().forEach(b -> System.out.println(b.getName()));

        //******************************************************************

        Query query2 = session2.createQuery("SELECT SUM(price) FROM Beverage");
        System.out.println("Price SUM " + query2.getSingleResult());

        Query query3 = session2.createQuery("SELECT AVG(capacity) FROM Beverage");
        System.out.println("Capacity AVG " + query3.getSingleResult());

        Query<Result> query4 = session2.createQuery("SELECT AVG(price), name FROM Beverage GROUP BY name");
        query4.getResultList().forEach(System.out::println);
        //System.out.println("Price AVG grouped by name " + query4.getResultList());

        transaction2.commit();
        session2.close();


        sessionFactory.close();

    }

    class Result {
        BigDecimal price;
        String name;

        @Override
        public String toString() {
            return "Result{" +
                    "price=" + price +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
