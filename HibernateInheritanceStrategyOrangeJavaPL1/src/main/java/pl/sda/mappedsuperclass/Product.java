package pl.sda.mappedsuperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@MappedSuperclass
public abstract class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    protected String name;

    @Column(columnDefinition = "DECIMAL(7,2)")
    protected BigDecimal price;

    public Product() {

    }

    public Product(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }
}
