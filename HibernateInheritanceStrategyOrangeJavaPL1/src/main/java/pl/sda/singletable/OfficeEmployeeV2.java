package pl.sda.singletable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue("OFFICE_EMPLOYEE")
@Setter
@Getter
@Entity
public class OfficeEmployeeV2 extends EmployeeV2 {

    private String skills;

    public OfficeEmployeeV2() {

    }

    public OfficeEmployeeV2(String firstName, String lastName, String skills) {
        super(firstName, lastName);
        this.skills = skills;
    }

}
