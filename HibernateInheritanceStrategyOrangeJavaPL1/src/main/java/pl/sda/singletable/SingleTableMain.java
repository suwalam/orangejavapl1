package pl.sda.singletable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.tableperclass.Director;

public class SingleTableMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV2 officeEmployee = new OfficeEmployeeV2("Jan", "Kowalski", "Excel");

        EmployeeV2 director = new DirectorV2("Michał", "Nowak", "IT");

        session.save(officeEmployee);
        session.save(director);

        DirectorV2 directorFromDB = session.get(DirectorV2.class, 2);
        System.out.println(directorFromDB.getLastName());

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
