package pl.sda.singletable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "employeeType")
@Entity
@Table(name = "employee_singletable")
public abstract class EmployeeV2 {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;

    protected String firstName;

    protected String lastName;

    @Column(insertable = false, updatable = false)
    protected String employeeType;

    public EmployeeV2() {

    }

    public EmployeeV2(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
