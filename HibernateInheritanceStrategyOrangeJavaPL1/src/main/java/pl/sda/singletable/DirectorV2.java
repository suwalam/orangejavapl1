package pl.sda.singletable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@DiscriminatorValue("DIRECTOR")
@Setter
@Getter
@Entity
public class DirectorV2 extends EmployeeV2 {

    private String department;

    public DirectorV2() {

    }

    public DirectorV2(String firstName, String lastName, String department) {
        super(firstName, lastName);
        this.department = department;
    }

}
