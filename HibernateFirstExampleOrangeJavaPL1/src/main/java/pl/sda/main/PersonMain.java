package pl.sda.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.sda.entity.Person;

public class PersonMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Person person = new Person(null, "Jan", "Kowalski", "78121212345");

        session.save(person);

        transaction.commit();
        session.close();

        Session session2 = sessionFactory.openSession();
//        Person personFromDB = session2.get(Person.class, 100); //zwraca null jeśli w bazie nie ma wiersza o podanym id
        Person personFromDB = session2.load(Person.class, 100); //rzuca wyjątek ObjectNotFoundException jeśli w bazie nie ma wiersza o podanym id
        System.out.println(personFromDB);



        sessionFactory.close();
    }
}
