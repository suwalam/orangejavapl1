package pl.sda.jdbc;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.*;

public class JDBCUtil {

    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/jdbcexampleorangejavapl1";

    private static final String USER = "root";

    private static final String PASSWORD = "root";

    /**
     * Alternatywny sposób na pozyskiwanie obiektu Connection, równoważny do DriverManager.getConnection()
     */
    public static Connection getConnection() throws SQLException {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(CONNECTION_URL);
        dataSource.setUser(USER);
        dataSource.setPassword(PASSWORD);
        return dataSource.getConnection();
    }

    public static void execute(String sql) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
            connection.createStatement().execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void executeQuerySelectFromPerson(String sql) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String pesel = resultSet.getString("pesel");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void executePreparedStatementSelectFromPerson(String sql, String name) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String pesel = resultSet.getString("pesel");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void executePreparedStatementUpdatePerson(String sql, String newLastName, String pesel) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, newLastName);
            preparedStatement.setString(2, pesel);
            int updatedRows = preparedStatement.executeUpdate();
            System.out.println("Zmieniono " + updatedRows + " wierszy");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void executePreparedStatementDeleteFromPerson(String sql, String pesel) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(CONNECTION_URL, USER, PASSWORD);
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, pesel);
            int updatedRows = preparedStatement.executeUpdate();
            System.out.println("Zmieniono " + updatedRows + " wierszy");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void executeStoredProcedureSelectAllFromPerson() {
        Connection connection = null;
        try {
            connection = getConnection();
            ResultSet resultSet = connection.prepareCall("{call selectFromPerson()}").executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String pesel = resultSet.getString("pesel");

                System.out.println(id + " " + firstName + " " + lastName + " " + pesel);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
