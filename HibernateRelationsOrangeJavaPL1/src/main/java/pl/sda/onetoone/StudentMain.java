package pl.sda.onetoone;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.time.LocalDate;

public class StudentMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        StudentIndex studentIndex = new StudentIndex(null, "12345", null);
        Student student = new Student(null, "Jan", "Kowalski", LocalDate.of(2000, 12,12), studentIndex);

        studentIndex.setStudent(student);

        session.save(student);
        session.save(studentIndex);

        transaction.commit();
        session.close();

        Session session2 = sessionFactory.openSession();
        Student studentFromDB = session2.get(Student.class, 1);
        System.out.println(studentFromDB);

        String lastName = "Kowalski";
        String selectFromStudent = "FROM Student s WHERE s.lastName = :p1";
        Query<Student> query = session2.createQuery(selectFromStudent, Student.class);
        query.setParameter("p1", lastName);
        System.out.println("query.getSingleResult() " + query.getSingleResult());

        session2.close();
        sessionFactory.close();

    }

}
