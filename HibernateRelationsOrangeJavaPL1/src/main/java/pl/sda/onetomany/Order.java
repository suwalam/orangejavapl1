package pl.sda.onetomany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(columnDefinition = "DECIMAL(7,2)")
    private BigDecimal totalAmount;

    private String products;

    private LocalDateTime date;

    @JoinColumn(name = "clientId")
    @ManyToOne
    private Client client;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", totalAmount=" + totalAmount +
                ", products='" + products + '\'' +
                ", date=" + date +
                ", client nick=" + client.getNick() +
                '}';
    }
}

