package pl.sda.onetomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

public class ClientMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Order order1 = new Order(null, BigDecimal.valueOf(112.43), "woda", LocalDateTime.now(), null);
        Order order2 = new Order(null, BigDecimal.valueOf(1432.65), "gaz", LocalDateTime.now(), null);

        Client client = new Client(null, "nick123", Arrays.asList(order1, order2));
        order1.setClient(client);
        order2.setClient(client);

        session.save(order1);
        session.save(order2);
        session.save(client);

        transaction.commit();
        session.close();

        Session session2 = sessionFactory.openSession();
        Transaction transaction2 = session2.beginTransaction();

        Client clientFromDB = session2.get(Client.class, 1);

//        System.out.println("Ilość zamówień przy trybie LAZY: " + clientFromDB.getOrders().size()); //0

        System.out.println("Client nick: " + clientFromDB.getNick());
        System.out.println("______________________________________");

        clientFromDB.getOrders().forEach(System.out::println);

        System.out.println("Ilość zamówień pp wywołaniu getOrders w trybie LAZY: " + clientFromDB.getOrders().size()); //2

        session2.remove(clientFromDB);

        transaction2.commit();
        session2.close();
        sessionFactory.close();

    }

}
