package pl.sda.compositekeys;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;

@IdClass(AccountId.class)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Account {

    @Id
    @Column(length = 26)
    private String number;

    @Id
    @Column(length = 20)
    private String type;

    @Column(columnDefinition = "DECIMAL(7,2)")
    private BigDecimal amount;
}
