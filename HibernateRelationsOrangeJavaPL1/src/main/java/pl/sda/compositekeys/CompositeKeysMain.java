package pl.sda.compositekeys;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;

public class CompositeKeysMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Account account = new Account("12345", "walutowe", BigDecimal.valueOf(1234.43));
        session.save(account);

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
