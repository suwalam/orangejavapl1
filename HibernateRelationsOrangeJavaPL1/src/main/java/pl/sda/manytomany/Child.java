package pl.sda.manytomany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Child {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String lastName;

    @JoinTable(name = "ChildParent",
            joinColumns = @JoinColumn(name = "childId", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "parentId", referencedColumnName = "id")
    )
    @ManyToMany
    private List<Parent> parents;

    @Override
    public String toString() {
        return "Child{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
