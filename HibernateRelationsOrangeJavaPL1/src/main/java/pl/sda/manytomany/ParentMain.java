package pl.sda.manytomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;

public class ParentMain {

    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Parent parent1 = new Parent(null, "Jan", "Kowalski", null);
        Parent parent2 = new Parent(null, "Anna", "Kowalska", null);

        Child child1 = new Child(null, "Kacper", "Kowalski", Arrays.asList(parent1, parent2));
        Child child2 = new Child(null, "Kasia", "Kowalska", Arrays.asList(parent1, parent2));

        parent1.setChildren(Arrays.asList(child1, child2));
        parent2.setChildren(Arrays.asList(child1, child2));

        session.save(child1);
        session.save(child2);
        session.save(parent1);
        session.save(parent2);

        Parent parentFromDB = session.get(Parent.class, 2);
        System.out.println(parentFromDB);

        Child childFromDB = session.get(Child.class, 1);

        System.out.println(childFromDB.getParents());

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
