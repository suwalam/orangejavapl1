package pl.sda.service;

import pl.sda.entity.Patient;

import java.time.format.DateTimeFormatter;

public abstract class AbstractServiceTest {

    protected String patientName = "Jan";
    protected String patientSurname = "Kowalski";
    protected String pesel = "89070612345";

    protected String doctorName = "Marek";
    protected String doctorSurname = "Nowak";
    protected String doctorIdentifier = "12345";

    protected DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

}
