package pl.sda.service;

import org.junit.jupiter.api.Test;
import pl.sda.entity.Patient;
import pl.sda.util.HibernateUtil;

import static org.junit.jupiter.api.Assertions.*;

public class PatientServiceTest {

    @Test
    public void shouldGetPatientByPesel() {
        //given
        PatientService underTest = new PatientService();
        String name = "Jan";
        String surname = "Kowalski";
        String pesel = "89070612345";
        Patient testPatient = new Patient(null, name, surname, pesel, null, null);
        underTest.save(testPatient);

        //when
        Patient patientByPesel = underTest.getByPesel(pesel);

        //then
        assertNotNull(patientByPesel);
        assertEquals(pesel, patientByPesel.getPesel());
        assertEquals(name, patientByPesel.getName());
        assertEquals(surname, patientByPesel.getSurname());

        HibernateUtil.closeSessionFactory();
    }

}