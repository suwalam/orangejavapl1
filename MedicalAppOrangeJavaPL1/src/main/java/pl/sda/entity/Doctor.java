package pl.sda.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String surname;

    private String identifier;

    @OneToMany(mappedBy = "doctor", fetch = FetchType.EAGER)
    private List<Appointment> appointments;

    @JoinTable(name = "join_doc_spec",
            joinColumns = @JoinColumn(name = "doctor_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "speciality_id", referencedColumnName = "id")
    )
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Speciality> specialities;

    public List<Appointment> getAppointments() {
        return appointments == null ? new ArrayList<>() : appointments;
    }

    public List<Speciality> getSpecialities() {
        return specialities == null ? new ArrayList<>() : specialities;
    }
}
