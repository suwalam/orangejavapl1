package pl.sda.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AppointmentDto {

    private LocalDateTime dateFrom;

    private String doctor;

    private String speciality;

}
