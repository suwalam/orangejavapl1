package pl.sda.service;

import pl.sda.dao.DoctorDao;
import pl.sda.dao.SpecialityDao;
import pl.sda.entity.Doctor;
import pl.sda.entity.Speciality;
import pl.sda.entity.SpecialityType;

import java.util.Arrays;

public class DoctorService {

    private DoctorDao doctorDao = new DoctorDao();

    private SpecialityDao specialityDao = new SpecialityDao();

    public boolean createDoctorWithSpeciality(String name, String surname, String identifier, SpecialityType... specialityTypes) {
        //sprawdź po identyfikatorze, czy lekarz już itnieje
        if (!doctorDao.existByIdentifier(identifier)) {
            //jeśli nie istnieje to utwórz obiekt encji Doctor, zapis do bazy danych
            Doctor doctor = new Doctor(null, name, surname, identifier, null, null);
            doctorDao.save(doctor);

            //każdą specjalność powiąż z encją doctor
            for (SpecialityType type : specialityTypes) {
                addSpeciality(identifier, type);
            }

            return true;
        }

        return false;
    }

    public void addSpeciality(String identifier, SpecialityType specialityType) {
        Speciality speciality = specialityDao.getByName(specialityType);
        Doctor doctor = doctorDao.getByIdentifier(identifier);

        if (speciality == null) {
            speciality = new Speciality(null, specialityType, Arrays.asList(doctor));
            specialityDao.save(speciality);
        } else {
            speciality.getDoctors().add(doctor);
            specialityDao.update(speciality);
        }

        doctor.getSpecialities().add(speciality);
        doctorDao.update(doctor);
    }

}
