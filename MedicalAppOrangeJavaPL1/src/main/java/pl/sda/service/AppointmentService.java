package pl.sda.service;

import pl.sda.dao.AppointmentDao;
import pl.sda.dao.DoctorDao;
import pl.sda.dao.PatientDao;
import pl.sda.entity.Appointment;
import pl.sda.entity.Doctor;
import pl.sda.entity.Patient;

import java.time.LocalDateTime;

public class AppointmentService {

    public static final int APPOINTMENT_DURATION_MINUTES = 20;

    private AppointmentDao appointmentDao = new AppointmentDao();

    private PatientDao patientDao = new PatientDao();

    private DoctorDao doctorDao = new DoctorDao();

    public boolean book(String patientPesel, String doctorIdentifier, LocalDateTime dateFrom) {
        LocalDateTime dateTo = dateFrom.plusMinutes(APPOINTMENT_DURATION_MINUTES);
        Patient patient = patientDao.getByPesel(patientPesel);
        Doctor doctor = doctorDao.getByIdentifier(doctorIdentifier);

        if (patient == null || doctor == null) {
            return false;
        }

        Appointment appointment = new Appointment(null, dateFrom, dateTo, false, patient, doctor);

        patient.getAppointments().add(appointment);
        doctor.getAppointments().add(appointment);

        appointmentDao.save(appointment);
        patientDao.update(patient);
        doctorDao.update(doctor);

        return true;
    }

}
