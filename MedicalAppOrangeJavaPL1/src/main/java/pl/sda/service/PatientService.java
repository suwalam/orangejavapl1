package pl.sda.service;

import pl.sda.dao.AddressDao;
import pl.sda.dao.PatientDao;
import pl.sda.dto.AppointmentDto;
import pl.sda.entity.Address;
import pl.sda.entity.Appointment;
import pl.sda.entity.Patient;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PatientService {

    private PatientDao patientDao = new PatientDao();

    private AddressDao addressDao = new AddressDao();

    public Patient getByPesel(String pesel) {
        return patientDao.getByPesel(pesel);
    }

    public void save(Patient patient) {
        patientDao.save(patient);
    }

    public boolean createPatientWithAddress(String name, String surname, String pesel, String city, String street, String number) {
        if (!patientDao.existsByPesel(pesel)) {
            Address address = new Address(null, city, street, number);
            Patient patient = new Patient(null, name, surname, pesel, address, null);

            addressDao.save(address);
            patientDao.save(patient);
            return true;
        }

        return false;
    }

    public List<AppointmentDto> getFutureAppointmentByPesel(String pesel) {
        List<Appointment> appointments = getByPesel(pesel)
                .getAppointments()
                .stream()
                .filter(a -> a.isCancelled() == false)
                .filter(a -> a.getDateFrom().isAfter(LocalDateTime.now()))
                .collect(Collectors.toList());

        List<AppointmentDto> appointmentDtos = new ArrayList<>();
        for (Appointment appointment : appointments) {
            AppointmentDto dto = new AppointmentDto(appointment.getDateFrom(),
                    appointment.getDoctor().getName() + " " + appointment.getDoctor().getSurname(),
                    appointment.getDoctor().getSpecialities().get(0).getName().toString());
            appointmentDtos.add(dto);
        }

        return appointmentDtos;

    }
}
