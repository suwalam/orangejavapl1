package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.entity.Patient;
import pl.sda.util.HibernateUtil;

import java.util.List;

public class PatientDao extends AbstractDao<Patient> {

    public PatientDao() {
        super(Patient.class);
    }

    public Patient getByPesel(String pesel) {
        String hql = "FROM Patient WHERE pesel = :p1";
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery(hql);
        query.setParameter("p1", pesel);

        List<Patient> resultList = query.getResultList();
        session.close();

        return resultList
                .stream()
                .findFirst()
                .orElse(null);
    }

    public boolean existsByPesel(String pesel) {
        return getByPesel(pesel) == null ? false : true;
    }

}
