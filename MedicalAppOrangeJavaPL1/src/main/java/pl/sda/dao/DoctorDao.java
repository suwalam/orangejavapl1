package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.entity.Doctor;
import pl.sda.util.HibernateUtil;

import java.util.List;

public class DoctorDao extends AbstractDao<Doctor> {

    public DoctorDao() {
        super(Doctor.class);
    }

    public Doctor getByIdentifier(String identifier) {
        String hql = "FROM Doctor WHERE identifier = :p1";
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery(hql);
        query.setParameter("p1", identifier);

        List<Doctor> resultList = query.getResultList();
        session.close();

        return resultList
                .stream()
                .findFirst()
                .orElse(null);
    }

    public boolean existByIdentifier(String identifier) {
        return getByIdentifier(identifier) == null ? false : true;
    }

}
