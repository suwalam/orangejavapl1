package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;
import pl.sda.entity.Doctor;
import pl.sda.entity.Speciality;
import pl.sda.entity.SpecialityType;
import pl.sda.util.HibernateUtil;

import java.util.List;

public class SpecialityDao extends AbstractDao<Speciality> {

    public SpecialityDao() {
        super(Speciality.class);
    }

    public Speciality getByName(SpecialityType specialityType) {
        String hql = "FROM Speciality WHERE name = :p1";
        Session session = HibernateUtil.getSession();
        Query query = session.createQuery(hql);
        query.setParameter("p1", specialityType);

        List<Speciality> resultList = query.getResultList();
        session.close();

        return resultList
                .stream()
                .findFirst()
                .orElse(null);
    }

    public boolean existsByName(SpecialityType specialityType) {
        return getByName(specialityType) == null ? false : true;
    }
}
