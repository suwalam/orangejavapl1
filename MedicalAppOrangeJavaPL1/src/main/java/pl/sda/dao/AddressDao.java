package pl.sda.dao;

import pl.sda.entity.Address;

public class AddressDao extends AbstractDao<Address> {

    public AddressDao() {
        super(Address.class);
    }

}
