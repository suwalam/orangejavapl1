package pl.sda.dao;

import pl.sda.entity.Appointment;

public class AppointmentDao extends AbstractDao<Appointment> {

    public AppointmentDao() {
        super(Appointment.class);
    }

}
