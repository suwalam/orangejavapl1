package pl.sda.main;

import pl.sda.dto.AppointmentDto;
import pl.sda.entity.SpecialityType;
import pl.sda.service.AppointmentService;
import pl.sda.service.DoctorService;
import pl.sda.service.PatientService;
import pl.sda.util.HibernateUtil;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;

public class MedicalAppMain {

    private static PatientService patientService = new PatientService();

    private static DoctorService doctorService = new DoctorService();

    private static AppointmentService appointmentService = new AppointmentService();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int choose;

        do {
            System.out.println("Wybierz opcję: ");
            System.out.println("0 - zakończ");
            System.out.println("1 - Zarejestruj pacjenta");
            System.out.println("2 - Zarejestruj lekarza");
            System.out.println("3 - Zarezerwuj wizytę");
            System.out.println("4 - Pokaż przyszłe wizyty pacjenta");

            choose = scanner.nextInt();

            switch (choose) {
                case 0: HibernateUtil.closeSessionFactory(); System.exit(0);
                case 1: registerPatient(); break;
                case 2: registerDoctorWithSpecialities(); break;
                case 3: bookAppointment(); break;
                case 4: getFutureAppointments(); break;
                default: System.out.println("Nierozpoznana operacja"); break;
            }
        } while (true);
    }

    private static void registerPatient() {
        System.out.println("Podaj imię: ");
        String name = scanner.next();

        System.out.println("Podaj nazwisko: ");
        String surname = scanner.next();

        System.out.println("Podaj PESEL: ");
        String pesel = scanner.next();

        System.out.println("Podaj miasto: ");
        String city = scanner.next();

        System.out.println("Podaj ulicę: ");
        String street = scanner.next();

        System.out.println("Podaj numer: ");
        String number = scanner.next();

        boolean result = patientService.createPatientWithAddress(name, surname, pesel, city, street, number);

        if (result) {
            System.out.println("Pomyślnie utworzono pacjenta o numerze PESEL: " + pesel);
        } else {
            System.out.println("Nie utworzono pacjenta o numerze PESEL: " + pesel);
        }
    }

    private static void registerDoctorWithSpecialities() {
        System.out.println("Podaj imię: ");
        String name = scanner.next();

        System.out.println("Podaj nazwisko: ");
        String surname = scanner.next();

        System.out.println("Podaj identyfikator: ");
        String identifier = scanner.next();

        System.out.println("Podaj specjalność lub wpisz BRAK: ");

        for (SpecialityType type : SpecialityType.values()) {
            System.out.println(type.name());
        }

        String specialityName = scanner.next();

        boolean result = doctorService.createDoctorWithSpeciality(name, surname, identifier, SpecialityType.valueOf(specialityName));

        if (result) {
            System.out.println("Pomyślnie utworzono lekarza o identyfikatorze: " + identifier);
        } else {
            System.out.println("Nie utworzono lekarza o identyfikatorze: " + identifier);
        }
    }

    private static void bookAppointment() {
        System.out.println("Podaj PESEL pacjenta: ");
        String pesel = scanner.next();

        System.out.println("Podaj identyfikator lekarza: ");
        String identifier = scanner.next();

        System.out.println("Podaj datę wizyty: ");
        String date = scanner.next();

        System.out.println("Podaj godzinę wizyty: ");
        String time = scanner.next();

        LocalDateTime dateFrom = LocalDateTime.parse(date + " " + time, HibernateUtil.DATE_TIME_FORMATTER);

        boolean result = appointmentService.book(pesel, identifier, dateFrom);
        if (result) {
            System.out.println("Pomyślnie utworzono wizytę");
        } else {
            System.out.println("Nie utworzono wizyty");
        }
    }

    private static void getFutureAppointments() {
        System.out.println("Podaj PESEL pacjenta: ");
        String pesel = scanner.next();

        List<AppointmentDto> futureAppointmentByPesel = patientService.getFutureAppointmentByPesel(pesel);

        for (AppointmentDto appointmentDto : futureAppointmentByPesel) {
            System.out.println("Data wizyty: " + appointmentDto.getDateFrom());
            System.out.println("Lekarz: " + appointmentDto.getDoctor());
            System.out.println("Specjalność: " + appointmentDto.getSpeciality());
        }
    }

}
