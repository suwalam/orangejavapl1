package pl.sda.parametrized;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class NumbersUtilTest {

    @ParameterizedTest
    @ValueSource(ints = {1, 63, 79, 57})
    public void shouldReturnTrueForOddNumbers(int input) {
        assertTrue(NumbersUtil.isOdd(input));
    }

    @ParameterizedTest
    @ValueSource(ints = {12, 634, 798, 570})
    public void shouldReturnFalseForEvenNumbers(int input) {
        assertFalse(NumbersUtil.isOdd(input));
    }

    @ParameterizedTest
    @MethodSource("provideNumbersWithInfoAboutParity")
    public void shouldVerifyParity(int input, boolean expected) {
        boolean result = NumbersUtil.isOdd(input);
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideNumbersWithInfoAboutParity() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(2, false),
                Arguments.of(13, true),
                Arguments.of(78, false)
        );
    }

    @ParameterizedTest
    @ArgumentsSource(NumbersWithParityArgumentsProvider.class)
    public void shouldVerifyParityWithArgumentsSource(int input, boolean expected) {
        boolean result = NumbersUtil.isOdd(input);
        assertEquals(expected, result);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenDividendIsZero() {
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> NumbersUtil.divide(10, 0))
                .withMessage("dividend can't be 0");
    }

}