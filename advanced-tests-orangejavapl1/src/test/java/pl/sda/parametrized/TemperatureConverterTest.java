package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class TemperatureConverterTest {

    private static final float ABSOLUTE_ZERO = -273.15F;

    @ParameterizedTest
    @EnumSource(value = TemperatureConverter.class, names = {"CELSIUS_KELVIN"}, mode = EnumSource.Mode.INCLUDE)
    public void shouldConvertTemperature(TemperatureConverter temperatureConverter) {
        System.out.println(temperatureConverter.name());
        float result = temperatureConverter.convertTemp(1);
        assertTrue(result > ABSOLUTE_ZERO);
    }

}