package pl.sda.parametrized;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.NullSource;

import static org.junit.jupiter.api.Assertions.*;

class StringsUtilTest {

    @ParameterizedTest
    //given
    @CsvSource({" test ,TEST", "JAva  ,JAVA", "java,JAVA"})
    public void shouldTrimAndUpperCaseInput(String input, String expected) {
        //when
        String actualValue = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actualValue);
    }

    //*************************************

    @ParameterizedTest
    //given
    @CsvSource(value = {" test ;TEST", "JAva  ;JAVA", "java;JAVA"}, delimiter = ';')
    public void shouldTrimAndUpperCaseInputWithDelimiter(String input, String expected) {
        //when
        String actualValue = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actualValue);
    }
    //*************************************

    @ParameterizedTest
    //given
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1, delimiter = ',', lineSeparator = ";", encoding = "UTF-8")
    public void shouldTrimAndUpperCaseInputFromCSVFile(String input, String expected) {
        //when
        String actualValue = StringsUtil.toUpperCase(input);

        //then
        assertEquals(expected, actualValue);
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void shouldBeBlankForEmptyOrNullInput(String input) {
        assertTrue(StringsUtil.isBlank(input));
    }


}