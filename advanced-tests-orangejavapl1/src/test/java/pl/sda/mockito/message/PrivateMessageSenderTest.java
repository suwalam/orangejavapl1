package pl.sda.mockito.message;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class PrivateMessageSenderTest {

    private static final String MESSAGE_TEXT = "hello";

    private static final String AUTHOR_ID = "Marek";

    private static final String RECIPIENT_ID = "Anna";

    @Mock
    private MessageProvider messageProvider;

    @Mock
    private MessageValidator messageValidator;

    @InjectMocks
    private PrivateMessageSender privateMessageSender;

    @Captor
    private ArgumentCaptor<Message> messageArgumentCaptor;

    @Test
    public void shouldSendPrivateMessage() {
        //given
        Mockito.when(messageValidator.isMessageValid(ArgumentMatchers.any(Message.class))).thenReturn(true);
        Mockito.when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);
        Mockito.doNothing().when(messageProvider).send(ArgumentMatchers.any(Message.class), ArgumentMatchers.eq(MessageType.PRIVATE));

        //when
        privateMessageSender.sendPrivateMessage(MESSAGE_TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        Mockito.verify(messageValidator).isMessageValid(ArgumentMatchers.any(Message.class));
        Mockito.verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        Mockito.verifyNoMoreInteractions(messageValidator);

        Mockito.verify(messageProvider).send(ArgumentMatchers.any(Message.class), ArgumentMatchers.eq(MessageType.PRIVATE));
        Mockito.verifyNoMoreInteractions(messageProvider);
    }


    @Test
    public void shouldSendPrivateMessageWithArgumentCaptor() {
        //given
        Mockito.when(messageValidator.isMessageValid(ArgumentMatchers.any(Message.class))).thenReturn(true);
        Mockito.when(messageValidator.isMessageRecipientReachable(RECIPIENT_ID)).thenReturn(true);
        Mockito.doNothing().when(messageProvider).send(ArgumentMatchers.any(Message.class), ArgumentMatchers.eq(MessageType.PRIVATE));

        //when
        privateMessageSender.sendPrivateMessage(MESSAGE_TEXT, AUTHOR_ID, RECIPIENT_ID);

        //then
        Mockito.verify(messageValidator).isMessageValid(messageArgumentCaptor.capture());
        Mockito.verify(messageValidator).isMessageRecipientReachable(RECIPIENT_ID);
        Mockito.verifyNoMoreInteractions(messageValidator);

        Mockito.verify(messageProvider).send(messageArgumentCaptor.capture(), ArgumentMatchers.eq(MessageType.PRIVATE));
        Mockito.verifyNoMoreInteractions(messageProvider);

        for (Message message : messageArgumentCaptor.getAllValues()) {
            System.out.println(message);
            Assertions.assertThat(message.getAuthor()).isEqualTo(AUTHOR_ID);
            Assertions.assertThat(message.getRecipient()).isEqualTo(RECIPIENT_ID);
            Assertions.assertThat(message.getValue()).isEqualTo(MESSAGE_TEXT);
            Assertions.assertThat(message.getSendAt()).isBefore(LocalDateTime.now());
            Assertions.assertThat(message.getId()).isNotNull();
        }

    }

}