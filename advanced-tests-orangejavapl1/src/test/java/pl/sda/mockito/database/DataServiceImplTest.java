package pl.sda.mockito.database;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.sda.mockito.exception.SdaException;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DataServiceImplTest {

    private static final Data TEST_DATA = new Data.DataBuilder().value("data element").build();

    @Mock
    private DatabaseConnection databaseConnection;

    @Spy
    private DataRepository dataRepository = new DataRepositoryImpl();

    @InjectMocks
    private DataServiceImpl dataService;

    @Test
    public void shouldAddDataWhenDatabaseConnectionIsOpened() {
        //given
        Mockito.when(databaseConnection.isOpened()).thenReturn(false, true, true);

        //when
        Data result = dataService.add(TEST_DATA);

        //then
        Assertions.assertThat(result).isEqualTo(TEST_DATA);
        Mockito.verify(databaseConnection, Mockito.times(3)).isOpened();
        Mockito.verify(databaseConnection).open();
        Mockito.verify(databaseConnection).close();
        Mockito.verifyNoMoreInteractions(databaseConnection);

        Mockito.verify(dataRepository).add(TEST_DATA);
        Mockito.verifyNoMoreInteractions(dataRepository);
    }

    @Test
    public void shouldThrowSdaExceptionWhenDatabaseConnectionIsNotOpened() {
        //given
        Mockito.when(databaseConnection.isOpened()).thenReturn(false, false);

        //when / then
        Assertions.assertThatExceptionOfType(SdaException.class)
                .isThrownBy(() -> dataService.add(TEST_DATA))
                .withMessage("Unable to open connection to the database");
        Mockito.verify(databaseConnection, Mockito.times(2)).isOpened();
        Mockito.verify(databaseConnection).open();
        Mockito.verifyNoMoreInteractions(databaseConnection);
        Mockito.verifyNoInteractions(dataRepository);

    }

}