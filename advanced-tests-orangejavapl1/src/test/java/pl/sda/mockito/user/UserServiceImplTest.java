package pl.sda.mockito.user;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final Long USER_ID = 1L;

    private static final User TEST_USER = new User(USER_ID, "Jan", "Kowalski");

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserValidator userValidator;

    //private UserService userService = new UserServiceImpl(userRepository, userValidator);

    //Równoważne powyższemu zapisowi
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void shouldGetUserById() {
        //given
        Mockito.when(userRepository.findById(USER_ID)).thenReturn(Optional.of(TEST_USER));

        //when
        User result = userService.getUserById(USER_ID);

        //then
        Assertions.assertThat(result).isEqualTo(TEST_USER);
        Mockito.verify(userRepository).findById(USER_ID);
        Mockito.verifyNoInteractions(userValidator);
    }

}